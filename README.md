# Advanced Git Workshop
Lab 13: Using .gitignore and .gitattributes

---

# Tasks

 - Creating a .gitignore file for a existent repository
 
 - Ignoring tracked files
 
 - Creating a .gitattributes file
 
 - Reducing merge conflicts using .gitattributes

---

## Preparations

 - Let's clone a repository to work with:
 
```
$ git clone https://oauth2:gkLjhh5W4te-cJngRBxB@gitlab.com/sela-git-advanced-workshop/static-web-app.git lab13
$ cd lab13
```

---

## Creating a .gitignore file for a existent repository

 - Create a .gitignore file in the repository root:
```
$ touch .gitignore
```

 - Add a rule to ignore the content of the img folder except .jpg files:
```
[Ii]mg/**/*.*
![Ii]mg/**/*.jpg
```

 - Add a rule to ignore the folder mail:
```
[Mm]ail/
```

 - Putting all together:
```
[Ii]mg/**/*.*
![Ii]mg/**/*.jpg
[Mm]ail/
```

---

## Ignoring tracked files

  - The .gitignore file don't affect tracked files so we will untrack all the repository and then track again only the desired files:
```
$ git rm -r --cached .
$ git add .
$ git commit -m "untrack files contained in the .gitignore file"
```

---

## Creating a .gitattributes file

 - Create a .gitattributes file in the repository root:
```
$ touch .gitattributes
```

 - Add a rule to handle endlines automatically:
```
* text=auto
```

 - Add a rule to manage conflicts in index.html using the ours strategy:
```
index.html merge=union
```

 - Putting all together:
```
* text=auto
index.html merge=union
```

 - Commit the .gitattributes file:
```
$ git add -A
$ git commit -m "add .gitattributes file"
```

 - Merge the changes in the feature branch to add the .gitattributes file
```
$ git checkout feature
$ git merge master -m "add .gitignore and .gitattributes"
```

---

## Reducing merge conflicts using .gitattributes

 - Add a description to the index.html head in the feature branch:
```
<!DOCTYPE html>
<html lang="en">

  <head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="FEATURE BRANCH DESCRIPTION">
    <meta name="author" content="">
```

 - Commit the changes:
```
$ git add -A
$ git commit -m "add description attribute"
```

 - Move to the master branch:
```
$ git checkout master
```

 - Add a description to the index.html head in the master branch:
```
<!DOCTYPE html>
<html lang="en">

  <head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="MASTER BRANCH DESCRIPTION">
    <meta name="author" content="">
```

 - Commit the changes:
```
$ git add -A
$ git commit -m "add description attribute"
```

 - Merge the feature branch into the master branch:
```
$ git merge feature -m "Automatic Merge Resolve"
```

 - Inspect the index.html file to see how the merge resolve the conflicts:
```
$ head -n 10 index.html
```
